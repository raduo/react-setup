# stage: 1 - the application build
FROM node:latest as react-build
WORKDIR /app
COPY . ./
RUN yarn install
RUN yarn build

# stage: 2 — the production environment
FROM nginx:alpine
COPY --from=react-build /app/build /usr/share/nginx/html
COPY default.conf /etc/nginx/conf.d/default.conf
EXPOSE 80 2222
CMD ["nginx", "-g", "daemon off;"]
