# React Project

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app) (for advanced configurations check the official website).<br>
All documentation commands must be executed inside a command line tool (IDE/OS terminal, cmd).<br>
The project is platform agnostic but UNIX based platforms are recommended for development.

## Technology Stack
* Latest React version (SPA library with virtual DOM support)
* React-Router for routing / application navigation
* Redux + Redux-Saga + Reselect for state management
* Styled-Components for application style
* PropTypes for runtime component properties validations

## Initial Project Setup
### Package Manager Installation
* Install the latest Yarn version from the [Yarn website](https://yarnpkg.com/en/docs/install).
* Or install the LTS version of [NodeJS](https://nodejs.org/en/). This will also install the NPM package manager.
Using NPM make a global install of Yarn package manager:
```sh
npm install -g yarn
```
### Project Folder Creation
* Development usage: install the latest version of [Git](https://git-scm.com/) and clone the repository:
```sh
git clone https://gitlab.com/oradu/react-setup.git
```
* Basic usage (non-development): download the repository and unzip the downloaded archive.

## Development Server
From the project folder install the project dependencies and start development server:
```sh
yarn
yarn start
```
If the browser does not automatically open, manually open it and go to http://localhost:3000/

## Testing
Start the test runner in order to execute existing application tests:
```sh
yarn test
```

## Production Build
Build a minified and optimized production version of the application inside the `build` folder:
```sh
yarn build
```
The generated `build` folder (or it's content) can be deployed/copied inside the public folder of any Web Server.
