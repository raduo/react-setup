module.exports = function({ env }) {
  const plugins = [];

  if (env === 'production') {
    plugins.push(['remove-test-ids', { attributes: ['data-test-id'] }]);
  }

  return {
    babel: {
      presets: [],
      plugins: plugins
    }
  };
};
