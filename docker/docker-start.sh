#!/usr/bin/env bash

# build docker image
echo "Stage 1: Build docker image"
docker build -t project-name:local ..

# start docker container
echo "Stage 2: Started docker container on port 80"
docker run -p 80:80 project-name:local
