import React from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import { Provider } from 'react-redux';
import { createGlobalStyle, ThemeProvider } from 'styled-components';
import AppContent from 'components/AppContent';
import AppFooter from 'components/AppFooter';
import rootStore from 'redux-modules';
import { defaultTheme } from 'themes';

const GlobalStyle = createGlobalStyle`
  html, body {
    height: 100%;
  }
  body {
    font-family: ${(props) => props.theme.fontFamily};
  }
  #root {
    min-height: 100%;
  }
`;

function App() {
  return (
    <ThemeProvider theme={defaultTheme}>
      <Router>
        <Provider store={rootStore}>
          <GlobalStyle />
          <AppContent />
          <AppFooter />
        </Provider>
      </Router>
    </ThemeProvider>
  );
}

App.propTypes = {};

export default App;
