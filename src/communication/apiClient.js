import errorHandler from './errorHandler';
import initRequest from './initRequest';

const DEFAULT_HEADERS = {
  'Content-Type': 'application/json',
};

function initApiRequest({ headers = DEFAULT_HEADERS, ...requestOptions }) {
  return initRequest({
    cache: 'no-store',
    headers: {
      Accept: 'application/json, text/plain',
      'Cache-control': 'no-cache, no-store',
      pragma: 'no-cache',
      ...headers,
    },
    ...requestOptions,
  });
}

function parseResponse(response) {
  if (response.ok) {
    return response.status === 204 ? { status: 'ok' } : response.json();
  } else {
    const error = {
      code: response.status,
      message: response.statusText,
    };
    throw error;
  }
}

class ApiClient {
  custom({ method, url, ...options }, failCallback) {
    return fetch(
      url,
      initApiRequest({
        method: method || 'POST',
        headers: {},
        ...options,
      })
    )
      .then(this.parseResponse)
      .catch((error) => {
        failCallback && failCallback(error);
        errorHandler.handle(error);
      });
  }
  delete(url, failCallback) {
    return fetch(
      url,
      initApiRequest({
        method: 'DELETE',
      })
    )
      .then(parseResponse)
      .catch((error) => {
        failCallback && failCallback(error);
        errorHandler.handle(error);
      });
  }
  get(url, failCallback) {
    return fetch(
      url,
      initApiRequest({
        method: 'GET',
      })
    )
      .then(parseResponse)
      .catch((error) => {
        failCallback && failCallback(error);
        errorHandler.handle(error);
      });
  }
  post(url, payload, failCallback) {
    return fetch(
      url,
      initApiRequest({
        body: JSON.stringify(payload),
        method: 'POST',
      })
    )
      .then(parseResponse)
      .catch((error) => {
        failCallback && failCallback(error);
        errorHandler.handle(error);
      });
  }
  put(url, payload, failCallback) {
    return fetch(
      url,
      initApiRequest({
        body: JSON.stringify(payload),
        method: 'PUT',
      })
    )
      .then(parseResponse)
      .catch((error) => {
        failCallback && failCallback(error);
        errorHandler.handle(error);
      });
  }
}

export default new ApiClient();
