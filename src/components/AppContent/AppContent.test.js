import React from 'react';
import { render } from '@testing-library/react';
import AppContent from './AppContent';

test('renders learn react link', () => {
  const { getByText } = render(<AppContent />);
  const linkElement = getByText(/learn react/i);
  expect(linkElement).toBeInTheDocument();
});
