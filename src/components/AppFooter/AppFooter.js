import React from 'react';
import PropTypes from 'prop-types';
import { Footer, Item, ItemsContainer, Section } from './style';

function AppFooter({ className, language }) {
  return (
    <Footer className={className}>
      <Section>
        <ItemsContainer spacing="1">
          <Item>App Language: {language}</Item>
        </ItemsContainer>
      </Section>
      <Section color="primary">
        <ItemsContainer spacing="1">
          <Item as="a" href="" target="_blank">
            Linkedin
          </Item>
          <Item as="a" href="" target="_blank">
            Xing
          </Item>
          <Item as="a" href="" target="_blank">
            Facebook
          </Item>
        </ItemsContainer>
      </Section>
    </Footer>
  );
}

AppFooter.propTypes = {
  className: PropTypes.string,
  language: PropTypes.string.isRequired,
};

export default AppFooter;
