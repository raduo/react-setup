import React from 'react';
import PropTypes from 'prop-types';
import { PDFObject } from './style';

function PDFViewer({ children, className, data, ...rest }) {
  return (
    <PDFObject className={className} data={data} type="application/pdf" {...rest}>
      {children}
    </PDFObject>
  );
}

PDFViewer.propTypes = {
  children: PropTypes.oneOfType([PropTypes.func, PropTypes.node, PropTypes.object]),
  className: PropTypes.string,
  data: PropTypes.string,
};

export default PDFViewer;
