import styled from 'styled-components';

export const PDFObject = styled.object`
  display: flex;
  min-height: 300px;
  width: 100%;
`;
