import React from 'react';
import ArrowLeft from './arrowLeft';

export default function ArrowDown({ ...rest }) {
  return <ArrowLeft {...rest} transform="rotate(-90)" />;
}
