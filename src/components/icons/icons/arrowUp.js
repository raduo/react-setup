import React from 'react';
import ArrowLeft from './arrowLeft';

export default function ArrowUp({ ...rest }) {
  return <ArrowLeft {...rest} transform="rotate(90)" />;
}
