import React from 'react';
import PropTypes from 'prop-types';
import { Redirect, Route } from 'react-router-dom';

function AuthRoute({ component: Component, isAuthenticated, ...rest }) {
  return (
    <Route
      {...rest}
      render={(routeProps) =>
        isAuthenticated ? (
          <Redirect
            to={routeProps.location.state ? routeProps.location.state.from : { pathname: '/' }}
          />
        ) : (
          <Component {...routeProps} />
        )
      }
    />
  );
}

AuthRoute.propTypes = {
  children: PropTypes.oneOfType([PropTypes.func, PropTypes.node, PropTypes.object]),
  component: PropTypes.oneOfType([PropTypes.func, PropTypes.node, PropTypes.object]),
  isAuthenticated: PropTypes.bool.isRequired,
};

export default AuthRoute;
