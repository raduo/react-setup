import { connect } from 'react-redux';
import * as authSelectors from 'redux-modules/auth/selectors';
import AuthRoute from './AuthRoute';

function mapStateToProps(state) {
  return {
    isAuthenticated: !!authSelectors.selectAuthToken(state),
  };
}

export default connect(mapStateToProps)(AuthRoute);
