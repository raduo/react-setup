import React from 'react';
import PropTypes from 'prop-types';
import { Redirect, Route } from 'react-router-dom';

function PrivateRoute({ children, component: Component, isAuthenticated, ...rest }) {
  return (
    <Route
      {...rest}
      render={(routeProps) =>
        isAuthenticated ? (
          children || <Component {...routeProps} />
        ) : (
          <Redirect
            to={{
              pathname: '/login',
              state: { from: routeProps.location },
            }}
          />
        )
      }
    />
  );
}

PrivateRoute.propTypes = {
  children: PropTypes.oneOfType([PropTypes.func, PropTypes.node, PropTypes.object]),
  component: PropTypes.oneOfType([PropTypes.func, PropTypes.node, PropTypes.object]),
  isAuthenticated: PropTypes.bool.isRequired,
};

export default PrivateRoute;
