/* prettier-ignore */
/* tslint:disable:quotemark */
export default {
  common: {
    button: {
      add: "Add",
      cancel: "Cancel",
      close: "Close",
      edit: "Edit",
      ok: "Ok",
      signIn: "Sign In",
      signUp: "Sign Up"
    },
    columnTitle: {
      status: "Status"
    },
    error: {
      http: {
        "40001": "Invalid Username or Password",
        "50301": "Server connection error"
      },
      validation: {
        const: {
          confirmPassword: "”Password” and “Confirm Password” must match",
        },
        dependencies: {
          default: "$t(error.validation.required.default)"
        },
        format: {
          default: "Invalid {{format}} format"
        },
        formatMinimum: {
          default: "Invalid value",
          dateTo: "End date must be after start date"
        },
        invalid: "Invalid data",
        minLength: {
          de: "$t(error.validation.required.de)",
          default: "$t(error.validation.required.default)"
        },
        pattern: {
          password: "Password must be at least 6 characters long and contain at least 1 uppercase character, 1 lowercase character, 1 numeric character and 1 special character",
          default: "$t(error.validation.invalid)"
        },
        required: {
          de: "German value is mandatory",
          default: "This field is required"
        },
        type: {
          default: "$t(error.validation.invalid)"
        }
      }
    },
    label: {
      acceptPrivacy : "Accept Privacy Policy",
      acceptTerms : "Accept Terms & Conditions",
      city: "City",
      comingSoon: "Coming Soon",
      confirmPassword: "Confirm Password",
      contactDetails: "Contact Details",
      country: "Country",
      description: "Description",
      details: "Details",
      email: "Email",
      emailAddress: "Email Address",
      fax: "Fax",
      firstName: "First Name",
      fullName: "Full Name",
      gender: "Gender",
      lastName: "Last Name",
      newPassword: "New password",
      password: "Password",
      phone: "Phone",
      street: "Street",
      zip: "Zip"
    },
    link: {
      backToLogin: "Back To Login",
      contact: "Contact",
      downloadPDF: "Download PDF",
      forgotPassword: "Forgot password?",
      home: "Home",
      legalNotice: "Legal notice",
      noAccountSignUp: "Don't have an account? Sign Up",
      privacyPolicy: "Privacy Policy"
    },
    menu: {
      loginTitle: "Login to your account",
      loginSettings: "Login Settings",
      logout: "Sign Out",
      myProfile: "My Profile",
      notificationSettings: "Notification Settings"
    },
    message: {
      activityDescription: "Please describe your activities here:",
      addMoreInformation: "Please add more information",
      introduceYourself: "Please introduce yourself in a few words",
      sectionIsEmpty: "This section is empty!",
      supportMessage: "If you still need help, please contact us at support@something.com",
    },
    placeholder: {
      profileImage: "Profile Image"
    },
    title: {
      resetPassword: "Reset password",
      signIn: "Sign In",
      updatePassword: "Change your password"
    }
  }
};
