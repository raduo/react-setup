export const loginSchema = {
  type: 'object',
  properties: {
    username: { $ref: 'defs.json#/definitions/email' },
    password: { $ref: 'defs.json#/definitions/password' },
  },
  required: ['password', 'username'],
};

export const forgotPasswordSchema = {
  type: 'object',
  properties: {
    email: { $ref: 'defs.json#/definitions/email' },
  },
  required: ['email'],
};

export const updatePasswordSchema = {
  type: 'object',
  properties: {
    password: {
      $ref: 'defs.json#/definitions/passwordCreate',
    },
    confirmPassword: {
      allOf: [
        {
          $ref: 'defs.json#/definitions/password',
        },
        {
          const: {
            $data: '1/password',
          },
        },
      ],
    },
  },
  required: ['password', 'confirmPassword'],
};

export const registerSchema = {
  type: 'object',
  properties: {
    email: { $ref: 'defs.json#/definitions/email' },
    firstName: {
      allOf: [{ $ref: 'defs.json#/definitions/name' }, { minLength: 1 }],
    },
    lastName: {
      allOf: [{ $ref: 'defs.json#/definitions/name' }, { minLength: 1 }],
    },
    password: { $ref: 'defs.json#/definitions/passwordCreate' },
  },
  required: ['email', 'firstName', 'lastName', 'password'],
};
