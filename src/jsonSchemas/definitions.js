export const defsSchema = {
  $id: 'defs.json',
  definitions: {
    email: {
      type: 'string',
      allOf: [{ transform: ['trim'] }, { format: 'email' }],
    },
    name: {
      type: 'string',
      allOf: [{ transform: ['trim'] }, { maxLength: 64 }],
    },
    password: {
      type: 'string',
      allOf: [{ transform: ['trim'] }, { minLength: 4 }],
    },
    passwordCreate: {
      type: 'string',
      allOf: [
        { transform: ['trim'] },
        {
          pattern:
            '^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[\\!@#$%^&*()\\\\[\\]{}\\-_+=~`|:;"\'<>,./?])(?=.{6,})',
        },
      ],
    },
    username: {
      type: 'string',
      allOf: [{ transform: ['trim'] }, { maxLength: 64, minLength: 1 }],
    },
  },
};
