import * as types from './types';

export function failRequest(error) {
  return {
    type: types.REQUEST_FAILED,
    payload: {
      error,
    },
  };
}

export function login(loginData) {
  return {
    payload: {
      loginData,
    },
    type: types.LOGIN,
  };
}

export function logout() {
  return {
    type: types.LOGOUT,
  };
}

export function register(registerData, successCallback, failCallback) {
  return {
    payload: {
      successCallback,
      failCallback,
      registerData,
    },
    type: types.REGISTER,
  };
}

export function resetPassword(resetPwdData, failCallback) {
  return {
    payload: {
      failCallback,
      resetPwdData,
    },
    type: types.RESET_PWD,
  };
}

export function setServerAuthToken(token) {
  return {
    payload: {
      token: token,
    },
    type: types.SERVER_AUTH_TOKEN,
  };
}
