import localStorage from 'local-storage-fallback';
import * as types from './types';

const initialState = {
  authToken: localStorage.getItem('authToken') || null,
  error: null,
  syncing: false,
};

function auth(state = initialState, action) {
  switch (action.type) {
    case types.LOGIN:
    case types.REGISTER:
    case types.RESET_PWD:
      return {
        ...state,
        error: null,
        syncing: true,
      };
    case types.LOGOUT:
      return {
        ...state,
        authToken: null,
      };
    case types.REQUEST_FAILED:
      return {
        ...state,
        error: action.payload.error,
        syncing: false,
      };
    case types.SERVER_AUTH_TOKEN:
      return {
        ...state,
        authToken: action.payload.token,
        error: null,
        syncing: false,
      };
    default:
      return state;
  }
}

export default auth;
