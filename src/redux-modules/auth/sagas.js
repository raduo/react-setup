import localStorage from 'local-storage-fallback';
import { call, put, takeEvery } from 'redux-saga/effects';
import apiClient from 'communication/apiClient';
import * as actions from './actions';
import * as types from './types';

export function logout() {
  localStorage.removeItem('authToken');
  localStorage.removeItem('refreshToken');
}

export function* loginUser({ authToken }) {
  localStorage.setItem('authToken', authToken);
  yield put(actions.setServerAuthToken(authToken));
}

export function* login(actionData) {
  try {
    const resp = yield call(
      apiClient.post,
      '/api/v1/login',
      actionData.payload.loginData,
      actionData.payload.failCallback
    );
    if (resp && resp.data) {
      yield* loginUser(resp.data);
    } else {
      yield put(actions.failRequest({ errorCode: resp.error.status }));
    }
  } catch (error) {
    yield put(actions.failRequest({ errorCode: 503 }));
  }
}

export function* register(actionData) {
  try {
    const resp = yield call(
      apiClient.post,
      '/api/v1/register',
      actionData.payload.registerData,
      actionData.payload.failCallback
    );
    if (resp.auth) {
      yield* actions.setServerAuthToken(resp);
    } else {
      yield put(actions.failRequest(resp));
    }
  } catch (error) {
    yield put(actions.failRequest('error'));
  }
}

export function* resetPassword(actionData) {
  try {
    const resp = yield call(
      apiClient.post,
      '/api/v1/reset-password',
      actionData.payload.resetPwdData,
      actionData.payload.failCallback
    );
    if (resp && resp.data) {
      yield put(actions.setServerAuthToken(resp));
    } else {
      yield put(actions.failRequest(resp));
    }
  } catch (error) {
    yield put(actions.failRequest('error'));
  }
}

export default [
  takeEvery(types.LOGIN, login),
  takeEvery(types.LOGOUT, logout),
  takeEvery(types.REGISTER, register),
  takeEvery(types.RESET_PWD, resetPassword),
];
