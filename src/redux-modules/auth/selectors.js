export const selectAuthToken = (state) => state.auth.authToken;
export const selectSyncState = (state) => state.auth.syncing;
export const selectSyncError = (state) => state.auth.error;
