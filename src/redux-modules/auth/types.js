export const LOGIN = 'login_is_initiated';
export const LOGOUT = 'user_logout';
export const REGISTER = 'register_is_initiated';
export const REQUEST_FAILED = 'auth_request_has_failed';
export const RESET_PWD = 'reset_pwd_initiated';
export const SERVER_AUTH_TOKEN = 'set_server_auth_token';
