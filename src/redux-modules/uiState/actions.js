import * as types from './types';

export function changeLanguage(language) {
  return {
    payload: {
      language: language,
    },
    type: types.CHANGE_LANGUAGE,
  };
}
