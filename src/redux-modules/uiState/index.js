import localStorage from 'local-storage-fallback';
import * as types from './types';

const initialState = {
  language: localStorage.getItem('language') || 'en',
};

function uiState(state = initialState, action) {
  let nextState;

  switch (action.type) {
    case types.CHANGE_LANGUAGE:
      nextState = {
        ...state,
        language: action.payload.language,
      };
      break;
    default:
      return state;
  }

  localStorage.setItem('uiState', nextState.language);

  return nextState;
}

export default uiState;
