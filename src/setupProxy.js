const { createProxyMiddleware } = require('http-proxy-middleware');

module.exports = function (app) {
  app.use(
    createProxyMiddleware('/api', {
      changeOrigin: true,
      cookieDomainRewrite: '',
      logLevel: 'debug',
      secure: false,
      target: 'http://localhost:8080',
    })
  );
  app.use(
    createProxyMiddleware('/ws', {
      target: 'ws://localhost:8081',
      ws: true,
    })
  );
};
