import { hexToRgba, lightenDarkenColor } from 'utils/colorHelpers';

const COLORS = {
  alabaster: '#F9F9F9',
  alto: '#D8D8D8',
  black: '#000000',
  cerulean: '#00A5ED',
  doveGrey: '#666666',
  dustyGrey: '#979797',
  iceberg: '#CCEFE8',
  lochmara: '#0088D2',
  persianGreen: '#00B18D',
  red: '#FF0000',
  sandyBeach: '#FFEECC',
  seashell: '#F1F1F1',
  silver: '#CCCCCC',
  tundora: '#444444',
  webOrange: '#FFA800',
  white: '#FFFFFF',
  wildSand: '#F5F5F5',
  yellowOrange: '#FFBE41',
  yourPink: '#FFCCCC',
};

const theme = {
  animation: {
    cubicBezier: '400ms cubic-bezier(0.23, 1, 0.32, 1) 0ms',
    standard: '200ms ease-out',
  },
  borderRadius: {
    xsmall: '1px',
    small: '2px',
    base: '4px',
    large: '8px',
    xlarge: '16px',
  },
  boxShadow: {
    base: `0 0 4px 1px ${hexToRgba(COLORS.black, 0.25)}`,
    large: `0 0 16px 1px ${hexToRgba(COLORS.black, 0.25)}`,
  },
  color: {
    /* default theme color used for base components state */
    default: {
      contrast: COLORS.white,
      dark: lightenDarkenColor(COLORS.tundora, -20),
      light: COLORS.doveGrey,
      main: COLORS.tundora,
    },
    /* primary theme color used for header and primary theme elements */
    primary: {
      contrast: COLORS.white,
      dark: lightenDarkenColor(COLORS.lochmara, -20),
      light: COLORS.cerulean,
      main: COLORS.lochmara,
    },
    /* secondary theme color used mainly for special items and actions */
    secondary: {
      contrast: COLORS.white,
      dark: lightenDarkenColor(COLORS.webOrange, -20),
      light: COLORS.yellowOrange,
      main: COLORS.webOrange,
    },
    background: {
      // main application background
      default: COLORS.wildSand,
      // background for dialogs and other overlays
      overlay: hexToRgba(COLORS.black, 0.3),
      // large containers or sections
      section: `linear-gradient(0deg, ${COLORS.wildSand} 0%, ${COLORS.white} 100%)`,
      // background for scrollable or small items
      surface: COLORS.white,
    },
    danger: {
      contrast: COLORS.white,
      dark: lightenDarkenColor(COLORS.red, -20),
      light: hexToRgba(COLORS.red, 0.7),
      main: COLORS.red,
    },
    disabled: {
      background: COLORS.wildSand,
      border: COLORS.silver,
      boxShadow: COLORS.silver,
      text: COLORS.silver,
    },
    grey: {
      100: COLORS.alabaster,
      200: COLORS.wildSand,
      300: COLORS.seashell,
      400: COLORS.alto,
      500: COLORS.silver,
      600: '',
      700: COLORS.dustyGrey,
      800: COLORS.doveGrey,
      900: COLORS.tundora,
    },
    success: {
      contrast: COLORS.white,
      dark: lightenDarkenColor(COLORS.persianGreen, -20),
      light: COLORS.iceberg,
      main: COLORS.persianGreen,
    },
    text: {
      highlight: COLORS.lochmara,
      hint: '',
      light: COLORS.doveGrey,
      main: COLORS.tundora,
    },
  },
  fontFamily: "Roboto, 'Helvetica Neue', Helvetica, sans-serif",
  fontSize: {
    xsmall: '12px',
    small: '14px',
    base: '16px',
    large: '20px',
    xlarge: '24px',
    xxl: '32px',
    huge: '48px',
  },
  fontWeight: {
    light: 300,
    normal: 400,
    bold: 700,
  },
  layer: {
    base: 0,
    above: 10,
    overlay: 100,
    modal: 1000,
  },
  opacity: {
    background: 0.88,
  },
  spacing: function (...args) {
    return args.map((multiplier) => `${multiplier * 8}px`).join(' ');
  },
};

export default theme;
